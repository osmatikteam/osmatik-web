/*	Mobile menu
	==================================================	*/
$(function() {
    var	btn_mobile   =  $('.Mainmenu-mobile'),
        menu 	     =  $('.Mainmenu').find('ul');

    btn_mobile.on('click', function(e) {
        e.preventDefault();

        var el  =  $(this);

        el.toggleClass('nav-active');
        menu.toggleClass('open-menu');
/*$(".Mainmenu-link").click(function () {
    $(".Mainmenu-list").css('max-height', 0)
});*/
    });
}); /*  <-- */




$(function(){
    //clic en un enlace de la lista
    $('ul li a').on('click',function(e){
        //prevenir en comportamiento predeterminado del enlace
        e.preventDefault();
        //obtenemos el id del elemento en el que debemos posicionarnos
        var strAncla=$(this).attr('href');

        //utilizamos body y html, ya que dependiendo del navegador uno u otro no funciona
        $('body,html').stop(true,true).animate({
            //realizamos la animacion hacia el ancla
            scrollTop: $(strAncla).offset().top
        },1000);
    });
});



$('.rubberBand').addClass('animated rubberBand');
$('.zoomIn').addClass('animated zoomIn');

